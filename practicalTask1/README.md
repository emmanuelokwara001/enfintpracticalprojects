# Practical task Create SQL Request
### 1. Remove all employees from the tenth department
### 2. Find the employee with the highest salary range (max_salary) and minimum (min_salary)
### 3. Output information from the database about the employee with the role of "President"
### 4. Get all the employees who are in London
<br/>
<br/>


# PROJECT
<p>The Project is an Api based project and the tasked are accessed through routes or endpoints</p>
<p>To Run the project see 

[link](#how-to-run)

</P>

<br/>
<br/>

<p>After Running Starting the server</p>
<br/>

* [view Employees](http://localhost:4000/employees)
* [view Highest Paid Employee](http://localhost:4000/employee/max_salary)
* [view Lowest paid Employee](http://localhost:4000/employee/min_salary)
* [view president](http://localhost:4000/employee/president)
* [view employees from London](http://localhost:4000/employees/london)
<br/>
<br/>

<p>You are able to delete employees based on their department Id</p>
<p>To use this feature you need to use your terminal</p>

* make sure that <code>curl</code> is installed if not.
* Run <code>sudo apt install curl -y</code> <b>Then</b> > <br><code>curl -X DELETE 'http://localhost:4000/employees/{employees ID }' </code>

# HOW-TO-RUN
* npm install
* npm run start



