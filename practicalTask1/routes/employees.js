const express = require('express');
const { db } = require("../models");
const { table } = require("console");
const { database } = require('pg/lib/defaults');


// employees routers 
const router = express.Router();


/**
 * Employees router
 * creates a GET request endpoint 
 * returns back a list of employees
 */
router.get('/employees' , ( req , res ) => {
    // Query execution withIn the database 
    db.query("SELECT * FROM employees" , handleQuery.bind( { res : res  } ) )
})


/**
 * creates a DELETE request end point 
 * deletes all employees with a particular department_id Inside of the database 
 */
router.delete('/employees/:department_id' , ( req , res ) => {

    // validates if the parameter is a valid parameter
    // if true it will execute the query String and
    // call the Handle query method to handle the response
    if ( !validateParam( req.params.department_id ) ){
        console.log( req.params.department_id )
        db.query( ` 
            DELETE FROM employees 
            WHERE department_id = 
            ${ parseInt(req.params.department_id ) }` 
            , handleQuery.bind( { res : res  })
            )

    }else{
        // if false 
        // sets the appropriate status
        //  returns the appropriate response
        res.status(404).json("Invalid Request");
    }

})



/**
 * creates a GET request end point
 * returns the employee with the highest salary
 */
router.get('/employee/max_salary' , ( req , res ) => {

    // retrieves the employee with the highest salary
    // responds to the user with the retrieved information
    db.query("SELECT * FROM employees ORDER BY salary desc LIMIT 1 " , handleQuery.bind({ res : res }))


})


/**
 * Creates a GET request end point
 * Returns the employee with the lowest salary
 */
router.get('/employee/min_salary' , ( req , res ) => {

    // Query execution 

    db.query("SELECT * FROM employees ORDER BY salary LIMIT 1 " , handleQuery.bind({ res : res }))

})


/**
 * creates a Get request endpoint
 * returns the president inside of the employees database 
 */

router.get('/employee/president' , ( req , res ) => {
    // makes a call to the database
    // calls handleQuery to handle the database query
    // as well as responding to the http request
    db.query("SELECT * FROM employees WHERE job_id=4" , handleQuery.bind( { res : res  }))
})


router.get('/employees/london' , ( req , res ) => {
    
    db.query( `select * from employees e , 
    (select d.department_id from departments d , 
    (select w.location_id from ( select * from locations
     where location_id = 2400) w) e where e.location_id =
      d.location_id) dd  where e.department_id = dd.department_id` , handleQuery.bind( { res : res }))
})


/**
 * Query handler callback 
 * Handles the response from the database 
 * Set's the appropriate HTTP status code 
 * Along with the required information
 */
function handleQuery( err , res ){
    if (err){
        // handles error response 
        this.res.status( 404 ).json( "Internal Error Please Try Again Later!!")
        return console.log( "An Error occurred while processing the query!👻")
    }
    // Logs the rows returned 
    // This is probably not the most efficient way
    table( res.rows )

    // returns the rows as json data 
    this.res.status(200).json( res.rows )

}


/**
 * 
 * @param { HTTP  parameter } parameter 
 * @returns true if parameter is valid else false 
 */
function validateParam( parameter ){
    // converts the parameter into a number or Integer
    const number = parseInt( parameter )

    // returns if the converted data is a Nan or not 
    return isNaN( number )
    
}

// exporting the router 
module.exports = { erouter : router } ;