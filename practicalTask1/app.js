const express = require('express');
const path = require('path');
const usersRouter = require('./routes/company');
const erouter = require('./routes/employees')
const dotenv = require('dotenv');
const employeesRouter = erouter.erouter ; 

dotenv.config();
const app = express();
const port = process.env.PORT || '4000'

app.set('port', port);

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/company', usersRouter);
app.use('/', employeesRouter );

app.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`)
})


